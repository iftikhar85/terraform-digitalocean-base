- first create token from your api/token and keys
- add your pub key to ur account: from your account/settings/security/add_key

```

curl -o ~/terraform.zip  https://releases.hashicorp.com/terraform/0.12.24/terraform_0.12.24_linux_amd64.zip
mkdir  ~/opt/terraform/bin
unzip ~/terraform.zip -d ~/opt/terraform/bin
nano ~/.bashrc
export PATH=$PATH:~/opt/terraform/bin
terraform

mkdir ~/apache2-proj
cd ~/apache2-proj

```

# 1 - create provider

$ vim provider.tf

```
variable "do_token" {}
variable "pub_key" {}
variable "pvt_key" {}
variable "ssh_fingerprint" {}

provider "digitalocean" {
  token = var.do_token
}

```

# 2-  create apche2

$ vim apache2-1.tf
```
resource "digitalocean_droplet" "apache2-1" {
    image = "ubuntu-18-04-x64"
    name = "apache2-1"
    region = "nyc1"
    size = "s-1vcpu-1gb"
    private_networking = true
    ssh_keys = [
      var.ssh_fingerprint
    ]

  connection {

    host = self.ipv4_address
    user = "root"
    type = "ssh"
    private_key = file(var.pvt_key)
    timeout = "2m"
  }
  provisioner "remote-exec" {
    inline = [
      "export PATH=$PATH:/usr/bin",
      # install apache2
      "sudo apt-get update",
      "sudo apt-get -y install apache2",
      "sudo systemctl start apache2"

    ]
  }
}

```
3 - create file contains your varaiables

$ vim terraform.tfvars

```
do_token = "your_secret_token_from_digital_ocean_account"
ssh_fingerprint = "f3:f2:9e:eb:15:21:a1:23:f2:4b:28:82:b5:ec:6c:fa"
pub_key = "AAAAB3NzaC1yc2EAAAADAQABAAACAQDXlV6YwyyiqJl+xjMQLhaeIIRS7jJwk/t4mm2NoFnauvv8sqSZLY0qNW9B9XJObIsa9bkJY6RXcYoQaujKXjJhWQN6cJf4KfPbHP7IeBtltaCW0DD/9dnULac82GuTeYzn0UWeJaL76qvbmXJBDdRj+3MaQ/o+AxJ7aQki75btMjn5c/ag8F0HxddL/3jlQ+fLwYLUDipLigU3MgdMaXMCqD73By3FhlP+a5Q7KZJPLNHj0DpoLETr55LCunxAHVWamAG8+wLqvI1JsUW/tofwYn37e7nHclrSDS4f6n8KMaBWH6M88N8SBMXMFuG9P/z4mobYX8/xrsflTOX8fUl0fYLs5nicGOD4uORLN1gGuOLQfND/AdzSF9hALSIn28bJrJrS7OaT3VItHfE3XiekNPUcEH46eVUmUJXrwrteoOvx6D6Q4+bqM2qUNKc5+TpkCVZS4Fs1ujO5Dt4YEYK5T9iQJQECnFZbXfVO+CqPsvLXzLccGLismx32lm2YlT1GAnWEM6e3hs1vYzPcobIBuJsuvWUfmSitA+X8m9t4rLQbdrwEFWi53pb3+iXJr3FV5ayePTFY3gROdUEf/dGVrca2yOFXfqus/D3locQnpE4zMWsXpagzFV328Vuqht5iq2TmAOjMDqdRQxPcHvOCn2IG73Z7NIP5scdNDvMICw== advocatediablo@mail.com"
pvt_key = "/root/.ssh/id_san3.pub"

```

# size is the vm compute size
# https://developers.digitalocean.com/documentation/changelog/api-v2/new-size-slugs-for-droplet-plan-changes/

# 3 - destory and init terraform

# 4 - terraform plan command to see what Terraform will attempt to do to build the infrastructure you described

# 5 - terraform apply command to execute the current plan.
```
$ terraform destroy -auto-approve && terraform init && terraform plan && terraform apply -auto-approve
```

# 6 - check the status of plan
```
$ terraform show terraform.tfstate
$ terraform show

```