resource "digitalocean_droplet" "apache2" {
    image = "centos-8-x64"
    name = "apache2"
    region = "nyc1"
    size = "s-1vcpu-1gb"
    private_networking = true
    ssh_keys = [
      var.ssh_fingerprint
    ]
    user_data = data.template_file.cloud-init-yaml.rendered
    # connection {
    #   host = self.ipv4_address
    #   user = "root"
    #   type = "ssh"
    #   private_key = var.pvt_key
    #   timeout = "2m"
    # }
    # provisioner "file" {
    # source      = "./user-data.sh"
    # destination = "/root/user-data.sh"
    # }
    # provisioner "remote-exec" {
    #   inline = [
    #     "export PATH=$PATH:/usr/bin",
    #     # install apache2
    #     "sudo yum update -y",
    #     "sudo yum install -y --nogpgcheck httpd curl",
    #     "sudo systemctl restart httpd",
    #     "chmod +x /root/user-data.sh; /root/user-data.sh"
    #   ]
    # }
}

data "template_file" "cloud-init-yaml" {
  template = file("${path.module}/cloud-init.yaml")
  # vars = {
  #   init_ssh_public_key = file(var.ssh_public_key)
  # }
}

resource "null_resource" "local" {
  triggers = {
    template = "${data.template_file.ips.rendered}"
  }

  provisioner "local-exec" {
    command = "echo \"${data.template_file.ips.rendered}\" > ${var.ips_filename}"
  }
}
