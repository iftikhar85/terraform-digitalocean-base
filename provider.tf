variable "do_token" {}
variable "pub_key" {}
variable "pvt_key" {}
variable "ssh_fingerprint" {}
variable "ips_filename" {}
provider "digitalocean" {
  token = var.do_token
}

data "template_file" "ips" {
  template = "${file("${path.module}/ips.tpl")}"
    vars = {
      HOSTNAME = digitalocean_droplet.apache2.name
      PUBLIC_IP = digitalocean_droplet.apache2.ipv4_address
      PRIVATE_IP = digitalocean_droplet.apache2.ipv4_address_private
    }
}
